import os

class BaseInhibitor(object):
    def __init__(self):
        self.inhibited = False

    def inhibit(self):
        pass

    def off(self):
        pass

    def cleanUP(self):
        pass


# class Windows(BaseInhibitor):
#     import ctypes

#     def __init__(self):
#         super().__init__()
#         self.ES_CONTINUOUS = 0x80000000
#         self.ES_DISPLAY_REQUIRED = 0x00000002
#         self.ES_SYSTEM_REQUIRED = 0x00000001

#     def inhibit(self):
#         self.inhibited = __class__.ctypes.windll. \
#         kernel32.SetThreadExecutionState(
#         self.ES_CONTINUOUS | self.ES_DISPLAY_REQUIRED | self.ES_SYSTEM_REQUIRED
#         )

#     def off(self):
#         if self.inhibited:
#             __class__.ctypes.windll.kernel32. \
#             SetThreadExecutionState(self.inhibited)
#             self.inhibited = False


# class OSX(BaseInhibitor):
#     import subprocess

#     def __init(self):
#         super().__init__()

#     def inhibit(self):
#         self.inhibited = __class__.subprocess.Popen(
#             'caffeinate -di',
#             shell=True
#         )

#     def off(self):
#         if self.inhibited:
#             self.inhibited.terminate()
#             self.inhibited = False


# class LightLocker(BaseInhibitor):
#     import subprocess

#     def __init__(self):
#         super().__init__()

#     def inhibit(self):
#         self.inhibited = __class__.subprocess.Popen(
#                         'light-locker-command -i',
#                         shell=True
#                         )

#     def off(self):
#         if self.inhibited:
#             self.inhibited.terminate()
#             self.inhibited = False


class Gnome(BaseInhibitor):
    import subprocess
    import pickle

    def __init__(self):
        super().__init__()
        self.state = os.path.join(
            # os.environ['HOME'], '.config', 'insomnio.state')
            '/tmp', f'{os.environ["USER"]}_insomnio.state'
        )

        # if we have a state ofject reset everything to that
        self.checkState()

        self.lock = __class__.subprocess.check_output(
            'gsettings get org.gnome.desktop.screensaver lock-enabled',
            shell=True
            ).decode().strip()
        self.idle_time = __class__.subprocess.check_output(
            'gsettings get org.gnome.desktop.session idle-delay',
            shell=True
            ).decode().strip().split()[1]

        # set the current state
        self.setState(self.lock, self.idle_time)

    def setState(self, lock, idle_time):
        'records the screen saver state to a state pickle'
        __class__.pickle.dump((lock, idle_time), open(self.state, 'wb'))

    def checkState(self):
        'check if a state picke exsists and resets things to it if it does'
        if os.path.exists(self.state):
            lock, idle_time = __class__.pickle.load(open(self.state, 'rb'))
            __class__.subprocess.call(
                'gsettings set org.gnome.desktop.screensaver lock-enabled {}'
                .format(lock), shell=True)
            __class__.subprocess.call(
                'gsettings set org.gnome.desktop.session idle-delay {}'
                .format(idle_time), shell=True)

    def inhibit(self):
        self.idle_time = __class__.subprocess.check_output(
            'gsettings get org.gnome.desktop.session idle-delay',
            shell=True
            ).decode().strip().split()[1]
        self.setState(self.lock, self.idle_time)

        self.inhibited = True
        if self.lock == 'true':
            __class__.subprocess.call(
                'gsettings set org.gnome.desktop.screensaver lock-enabled false',
                shell=True
                )
        __class__.subprocess.call(
            'gsettings set org.gnome.desktop.session idle-delay 0',
            shell=True
            )

    def off(self):
        self.inhibited = False
        if self.lock == 'true':
            __class__.subprocess.call(
                'gsettings set org.gnome.desktop.screensaver lock-enabled true',
                shell=True
                )
        __class__.subprocess.call(
            'gsettings set org.gnome.desktop.session idle-delay {}'
            .format(self.idle_time),
            shell=True
            )

    def cleanUP(self):
        os.remove(self.state)


# inhibitors = {
#     'l': 'LightDM / light-locker',
#     'g': 'Ubuntu/GNOME/GDM',
#     'o': 'OSX',
#     'w': 'Windows'
# }

# inhibitorClasses = {
#     'l': 'LightLocker',
#     'g': 'Gnome',
#     'o': 'OSX',
#     'w': 'Windows'
# }
