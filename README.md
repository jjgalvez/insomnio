Insomnio
===================

Insomnio is a simple tray application that will keep your computer from
sleeping.

Insomnio, which is insomnia in Spanish is a pyqt based tray application
which will, if you activate it keep your computer awake. It was inspired
by keep\_awake and caffeine, both very fine programs. I had used the
caffeine gnome-shell extension for years, but after a switch to Arch and
LightDM with light-locker the caffeine extension no longer worked. I
decided to write a cross DE cross platform application that would
disable the screensaver allowing the user to configure the appropriate method.

Insomnio does not try to guess what to use, but rather lets the user
configure the proper way to inhibit the screensaver.

Currently insomnio support light-locker, gnome/GDM, OSX and Windows.
Freedesktop is in progress

## Usage:

insomnio\_config: configures the proper screensaver inhibitor

insonmio: starts the tray application which is then activated either
with a

    -   single click -- menu choice
    -   double click / middle click toggle

## Requirements:
 - Python3
 - PyQt5
 - click

## Install:
### Linux
#### Arch
Download latest pacman package from https://bintray.com/jjgalvez/Cybergalvez_binaries/insomnio
and install pacman -U insomnio-git-1.0-any.pkg.tar.xz  
or  
from AUR:  
<AUR Helper> -S insomnio-git

#### DEB
Download latest DEB package from https://bintray.com/jjgalvez/Cybergalvez_binaries/insomnio
and install apt install ./insomnio-git_amd64.deb  
or from the cybergalvez bintray deb repository  
deb https://dl.bintray.com/jjgalvez/cybergalvez ubuntu main  
signing key https://bintray.com/user/downloadSubjectPublicKey?username=bintray  

### Windows and OSX
Either clone the repository, install the dependencies and run insomnio.py or
download the binary package from Cybergalvez_binaries: https://bintray.com/jjgalvez/Cybergalvez_binaries/insomnio



 ## Notes:
 Both the Windows and OSX binaries contain software from [Python](https://www.python.org/),
 [Riverbank](https://riverbankcomputing.com/software/pyqt/intro), and [QT](https://www.qt.io/)
