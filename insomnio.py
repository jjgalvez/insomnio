#!/usr/bin/env python3
import sys
import os

from PyQt5.QtGui import QIcon

from PyQt5.QtWidgets import (
    QApplication, 
    QMainWindow,
    QSystemTrayIcon,
    QMenu, 
    QAction
   )

from inhibitors import Gnome as gnomeInhibitor


class MainWindow(QMainWindow):
    """
        Insomnio as a simple tray app
    """
    tray_icon = None

    # Override the class constructor
    def __init__(self):
        # Be sure to call the super class method
        super().__init__()

        # chdir to the right folder so relative paths work
        os.chdir(os.path.dirname(os.path.realpath(__file__)))

        self.inhibitor = gnomeInhibitor()

        self.setWindowTitle("Insomnio")  # Set a title
       
        # Init QSystemTrayIcon
        self.tray_icon = QSystemTrayIcon(self)
        
        '''
            Define and add steps to work with the system tray icon
            insomnio_on - turn insomnio on - screensaver off
            insomnio_off - turn insomnio off - screensaver on
            exit - exit properly restoring the screen saver
        '''

        insomnio_on = QAction("Insomnio On", self)
        insomnio_off = QAction("Insomnio Off", self)
        insomnio_on.triggered.connect(self.insomnioOn)
        insomnio_off.triggered.connect(self.insomnioOff)
        quit_action = QAction("Exit", self)
        quit_action.triggered.connect(self.close)
        tray_menu = QMenu()
        tray_menu.addAction(insomnio_on)
        tray_menu.addAction(insomnio_off)
        tray_menu.addAction(quit_action)
        self.tray_icon.setContextMenu(tray_menu)
        self.insomnioOn()
        self.tray_icon.show()

    # Override closeEvent, to make sure we clean up after ourselves
    def closeEvent(self, event):
        self.inhibitor.off()
        self.inhibitor.cleanUP()
        print('close event')

    def insomnioQuit(self):
        print('exit')
        self.close()


    def insomnioOn(self):
        self.tray_icon.setIcon(
            QIcon('resources/screensaverOff.png')
        )
        self.inhibitor.inhibit()
        print('on')

    def insomnioOff(self):
        self.tray_icon.setIcon(
            QIcon('resources/screensaverOn.png')
        )
        self.inhibitor.off()
        print('off')


if __name__ == "__main__":
    app = QApplication(sys.argv)
    mw = MainWindow()
    sys.exit(app.exec())
