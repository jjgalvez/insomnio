#!/usr/bin/env python3

import sys
import os
import time

from PyQt5.QtGui import (
    QIcon,
    QPixmap
)

from PyQt5.QtWidgets import (
    QApplication,
    # QMainWindow,
    # QAction,
    QWidget,
    QLabel,
    QPushButton,
    QGridLayout
)


from utils import (
    configFileCheck,
)

from config import getInhibitor

inhibitor = getInhibitor()


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        grid = QGridLayout()
        self.setLayout(grid)
        self.setWindowTitle('Insomnio')
        self.setWindowIcon(QIcon('resources/screensaverOff.png'))
        self.setStyleSheet(open('resources/stylesheet.css', 'r').read())
        label = QLabel(self)
        pixmap = QPixmap('resources/insomnioOff.png')
        label.setPixmap(pixmap)
        self.resize(pixmap.width(), pixmap.height())
        grid.addWidget(label, 2,0)
        self.button = QPushButton('To restore screensaver Click to close Insomnio')
        grid.addWidget(self.button, 1,0)
        # self.button.move(100, 0)
        self.button.clicked.connect(self.closeApp)
        print('Inhibiting Screensaver')
        inhibitor.inhibit()
        self.show()
        time.sleep(0.5)
        self.showMinimized()

    def closeApp(self, event):
        self.close()

    def closeEvent(self, event):
        print('Resoring screensaver')
        inhibitor.off()


os.chdir(os.path.dirname(os.path.realpath(__file__)))
configFileCheck()
main = QApplication(sys.argv)
window = MainWindow()
# window.show()
# window.showMinimized()
sys.exit(main.exec_())
