#!/usr/bin/env python3

import click
import os, sys
from inhibitors import inhibitors, inhibitorClasses
from config import configFile


def makePrompt():
    msg = 'Please select a screen saver to inhibit:\n'
    for k in inhibitors.keys():
        msg += '{}: {}\n'.format(k, inhibitors[k])
    msg += 'q: Quit\n'
    return msg


@click.command()
@click.option('--screensaver',
                prompt=makePrompt(),
                type=click.Choice(list(inhibitors.keys())+['q']),
                )
def writeConfig(screensaver):
    if screensaver.lower().strip() == 'q':
        sys.exit()
    open(configFile(), 'w').write(inhibitorClasses[screensaver])


if __name__ == '__main__':
    writeConfig()
