from PyQt5.QtGui import (
    QIcon,
    )

from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QMessageBox,
    QSystemTrayIcon
)

from whichos import *

from config import configFile
import time
import sys
import os


def setIcon(app, tray, icon):
    '''
    some GUIs need a dock icon, at least until I figure out how to
    eliminate the neeed.
    OSX needs a dock icon otherwise I end up with an ugly python icon
    others may need to addedk
    '''
    tray.setIcon(icon)
    if IS_OSX():
        app.setWindowIcon(icon)


def configFileCheck():
    '''
    Check to see if the config file is present.
    if not show error mesage and exit
    '''
    if not os.path.exists(configFile()):
        msg = 'config file not found, please run insomnio_config'
        showErrorMSG(msg)


def TrayCheck():
    '''
    check to see if a system tray is present.
    if not exit. This necessary because gnome no longer
    has one by default.
    '''
    time.sleep(30)
    a = QApplication([])  # make a temp app
    t = QSystemTrayIcon()
    trayPresent = isTrayPresent(t)
    del a
    del t
    if not trayPresent:
        msg = 'System tray can not be found, application timed out'
        showErrorMSG(msg)


def isTrayPresent(t):
    # time.sleep(5)
    'if tray is present return true otherwise return false. Timeout in 10 min'
    timer = 600  # 10 min time Timeout
    res = False  # default result
    while timer:
        available = t.isSystemTrayAvailable()
        print(available)
        if available:
            timer = 0
            res = True
            break
        time.sleep(2)
        timer -= 1

    # del a
    # del t
    return res


def showErrorMSG(msg):
    app = QApplication([])
    win = QMainWindow()
    win.setWindowIcon(QIcon('./resources/insomnio128.png'))
    QMessageBox().about(win, 'Error', msg)
    print(msg)
    sys.exit()
