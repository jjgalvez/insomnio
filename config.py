import inhibitors
import os
import sys
from whichos import *


def configFile():

    if (IS_OSX() | IS_LINUX()):
        HOME = os.environ['HOME']
        if not os.path.exists(os.path.join(HOME, '.config')):
            os.makedirs(os.path.join(HOME, '.config'))
        configFile = os.path.join(
            HOME,
            '.config',
            'insomnio.conf'
        )
    elif IS_WINDOWS():
        HOME = os.getenv('LOCALAPPDATA') if os.getenv('LOCALAPPDATA', False) else os.get('APPDATA')
        if not os.path.exists(os.path.join(HOME, 'Insomnio')):
            os.makedirs(os.path.join(HOME, 'Insomnio'))
        configFile = os.path.join(
            HOME,
            'Insomnio',
            'insomnio.conf'
        )
    else:
        sys.exit()
    return configFile


def getInhibitor():
    if os.path.exists(configFile()):
        i = open(configFile(), 'r').readline().strip()
        inhibitor = getattr(inhibitors, i)()
    else:
        inhibitor = False
    return inhibitor
