import sys


def IS_LINUX():
    return True if sys.platform.startswith('linux') else False


def IS_OSX():
    return True if sys.platform.startswith('darwin') else False


def IS_WINDOWS():
    return True if sys.platform.startswith('win') else False
